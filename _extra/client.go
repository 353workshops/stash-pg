package client

import (
	"context"
	"fmt"
	"net/http"
)

type Client struct {
	baseURL string
	c       http.Client
}

func New(baseURL string) *Client {
	c := Client{baseURL: baseURL}
	return &c
}

func (c *Client) Health(ctx context.Context) error {
	url := fmt.Sprintf("%s/health", c.baseURL)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	resp, err := c.c.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s - bad status: %s", url, resp.Status)
	}

	return nil
}
