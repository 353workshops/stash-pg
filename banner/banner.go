package main

import (
	"fmt"
	"math"
	"unicode/utf8"
)

/*
banner("Go", 6)

	  Go
	------
*/
func banner(text string, width int) {
	// fmt.Println("DEBUG:", text, len(text))
	/*
		var offset int
		offset = (width - len(text)) / 2
	*/

	// offset := (width - len(text)) / 2
	offset := (width - utf8.RuneCountInString(text)) / 2
	// i's scope is only the "for" loop
	// fmt.Print(strings.Repeat(" ", offset))
	for i := 0; i < offset; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)

	// fmt.Println(strings.Repeat("-", width))
	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}

// character ~ code point ~ rune
/*
// runtime/string.go
type stringStruct struct {
	str unsafe.Pointer // slice (array) of bytes in UTF-8
	len int  // length in bytes
}
*/

/* string duality
sequence of bytes
	- len
	- text[2]
sequence of runes
	- for range
*/

func main() {
	banner("Go", 6)
	banner("G☺", 6)

	s := "G☺!"
	fmt.Println("len:", len(s)) // 5 (in bytes)
	fmt.Println(s[2])           // byte
	for i, c := range s {
		// fmt.Println(i, ":", c) // runes
		fmt.Printf("%d: %c\n", i, c) // runes
	}

	fmt.Printf("pi = %.2f\n", math.Pi)

	id1, id2 := "1", 1
	fmt.Printf("%v == %v\n", id1, id2)   // for users
	fmt.Printf("%#v == %#v\n", id1, id2) // for developers

}
