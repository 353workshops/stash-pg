package main

import "fmt"

var n = 7

func main() {
	n := 9
	fmt.Println(n) // n from line 8
	fmt.Println(len([]int{1, 2}))
	{
		n := 10
		fmt.Println(n) // n from line 12
	}
	fmt.Println(n) // n from line 8

	inc := makeAdder(1)
	fmt.Println(inc(99))

	var calc []func()
	for i := 0; i < 10; i++ {
		i := i
		click := func() {
			fmt.Println("click on", i)
		}
		calc = append(calc, click)
	}
	// can't access i from here
	calc[3]()
}

func makeAdder(n int) func(int) int {
	return func(val int) int {
		return val + n // n comes from the closure
	}
}
