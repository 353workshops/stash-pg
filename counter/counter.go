package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/*
		var mu sync.Mutex
		count := 0
	*/
	count := int64(0)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for n := 0; n < 1000; n++ {
				time.Sleep(time.Microsecond)
				atomic.AddInt64(&count, 1)
				/*
					mu.Lock()
					count++
					mu.Unlock()
				*/
			}
		}()
	}

	wg.Wait()
	fmt.Println(count)

}
