package main

import (
	"errors"
	"fmt"
)

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
	/*
		fmt.Println(div(7, 3))
		fmt.Println(div(7, 0))
	*/
	fmt.Println("fin")
}

var ErrZeroDivision = errors.New("division by zero")

// q & err are named return values
// also good for documenting return values
func safeDiv(a, b int) (q int, err error) {
	// q & err are local variables in safeDiv
	// (same as a & b)
	defer func() {
		if e := recover(); e != nil {
			// convert e of type any to error
			// err = fmt.Errorf("%s", e)
			err = ErrZeroDivision
		}
	}()

	// happy path, inside "try" in other languages
	return div(a, b), nil
}

// written in external package
func div(a, b int) int {
	return a / b
}
