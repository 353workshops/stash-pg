package main

import (
	"fmt"
)

func main() {
	var a any // interface{}

	a = 8
	fmt.Printf("%v (%T)\n", a, a)

	a = "hi"
	fmt.Printf("%v (%T)\n", a, a)

	s := a.(string) // type assertion
	fmt.Println(s)

	// i := a.(int) // will panic
	if i, ok := a.(int); ok {
		fmt.Println(i)
	} else {
		fmt.Println("not an int")
	}

	Print(a)

	// n := nil
	var n error // = nil
	fmt.Printf("%#v\n", n)
}

// What fmt package does
func Print(v any) {
	if s, ok := v.(fmt.Stringer); ok {
		fmt.Print(s.String())
		return
	}

	switch v.(type) { // Type switch
	case int:
		fmt.Println("an int")
	case string:
		fmt.Println("a string")
	}
}
