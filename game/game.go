package main

import "fmt"

func main() {
	var loc Location
	fmt.Println("loc:", loc)
	fmt.Printf(" v: %v\n", loc)
	fmt.Printf("+v: %+v\n", loc)
	fmt.Printf("#v: %#v\n", loc)

	loc = Location{10, 20}
	fmt.Printf("#v: %#v\n", loc)
	loc = Location{Y: 10} //, X: 20}
	fmt.Printf("#v: %#v\n", loc)

	loc.X = 20
	fmt.Printf("#v: %#v\n", loc)

	fmt.Println(NewLocation(10, 20))
	fmt.Println(NewLocation(10, 2000))

	loc.Move(200, 300)
	fmt.Println("loc (move):", loc)

	p1 := Player{
		Name:     "Parzival",
		Location: Location{0, 0},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Println("p1.X", p1.X)
	p1.Move(78, 96)
	fmt.Printf("p1: %#v\n", p1)
	fmt.Println("p1.X", p1.X)
	fmt.Println("p1.Location.X", p1.Location.X)

	ms := []Mover{
		&loc,
		&p1,
	}
	MoveAll(maxX/2, maxY/2, ms)
	fmt.Printf("p1: %#v\n", p1)
	fmt.Printf("loc: %#v\n", loc)

	p1.Found(Copper)
	p1.Found(Copper)
	fmt.Println("keys:", p1.Keys) // [copper]
}

/* Exercise:
- Add a Keys field to player which is a slice of strings
- Add a "Found(key string) error" method to Player
	- It should err if key is not one of "jade", "copper", "crystal"
	- It should add a specific key only once
*/

func MoveAll(x, y int, ms []Mover) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

// String implements fmt.Stringer
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

// Go's Enum
const (
	Copper Key = iota + 1
	Jade
	Crystal
)

// Interface say what we need, not what we provide
type Mover interface {
	// Move(int, int)
	Move(x, y int)
}

/* Thought experiment: How do you sort without generics?
type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func Sort(s Sortable) {
	// ...
	s.Less(7, 8)
}
*/

func validKey(key Key) bool {
	switch key {
	case Copper, Jade, Crystal:
		return true
	}

	return false
}

func (p *Player) Found(key Key) error {
	if !validKey(key) {
		return fmt.Errorf("unknown key: %q", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}
	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if k == key {
			return true
		}
	}
	return false

}

type Player struct {
	Name string
	Keys []Key
	// X        float64 // Will "shadow" the Location.X
	Location // Player embeds location
}

// Move is a method
// "l" is called the receiver (self, this)
// Use pointer receiver if you want to mutate
func (l *Location) Move(x, y int) {
	l.X = x
	l.Y = y
}

// func NewLocation(x, y int) Location
// func NewLocation(x, y int) (Location, error)
// func NewLocation(x, y int) *Location
func NewLocation(x, y int) (*Location, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("Location %d/%d out of range %d/%d", x, y, maxX, maxY)
	}

	loc := Location{
		X: x,
		Y: y,
	}
	return &loc, nil // go is doing escape analysis and will alocate on heap
}

const (
	maxX = 1000
	maxY = 600
)

type Location struct {
	X int
	Y int
}
