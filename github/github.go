package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	name, numRepos, err := githubInfo(ctx, "ardanlabs")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println("name:", name, "num repos:", numRepos)
	/*
		name, numRepos, err := githubInfo("ardanlabs")
		fmt.Println(name, numRepos, err)
	*/

	/*
		// GoLand: "github/response.json"
		file, err := os.Open("response.json")
		if err != nil {
			log.Fatalf("error: %s", err)
		}

		if _, _, err := decodeJSON(file); err != nil {
			log.Fatalf("error: can't decode - %s", err)
		}
	*/
}

/* JSON

# JSON <-> Go Types
string <-> string
true/false <-> true/false (bool)
null <-> nil
number <-> float64, float32, int, int8, int16, int32, int64, uint, uint8 ...
array <-> []any, []int, ...
object <-> map[string]any, struct

# encoding/json

JSON -> io.Reader -> Go: json.NewDecoder
Go -> io.Writer -> JSON: json.NewEncoder
JSON -> []byte -> Go: json.Unmarshal
Go -> []bytes -> JSON: json.Marshal
*/

/*
type reply struct {
	Name     string `json:"name,omitempty"`
	NumRepos int    `json:"public_repos,omitempty"`
}
*/

func decodeJSON(r io.Reader) (string, int, error) {
	var reply struct { // anonymous struct
		Name     string `json:"name,omitempty"`
		NumRepos int    `json:"public_repos,omitempty"`
	}
	// var rp reply
	dec := json.NewDecoder(r)
	/*
		err := dec.Decode(&rp)
		if err != nil {
	*/
	if err := dec.Decode(&reply); err != nil {
		return "", 0, err
	}
	// fmt.Printf("name: %#v, repos: %d\n", reply.Name, reply.Public_Repos)
	return reply.Name, reply.NumRepos, nil
}

// githubInfo returns user name and number of public repos for login.
func githubInfo(ctx context.Context, login string) (string, int, error) {
	url := fmt.Sprintf("https://api.github.com/users/%s", url.PathEscape(login))
	// resp, err := http.Get(url)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	}

	if resp.StatusCode != http.StatusOK {
		return "", 0, err
	}

	//fmt.Println("content-type:", resp.Header.Get("Content-Type"))
	// io.Copy(os.Stdout, resp.Body)
	/* Go initializes variables to the zero value for the type
	var s string // ""
	var i int // 0
	*/

	// FIXME: Use (and change decodeJSON) here to return name and number of public repos
	// Instead of the io.Copy above
	return decodeJSON(resp.Body)
}
