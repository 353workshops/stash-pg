package main

import (
	"fmt"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 5; i++ {
		// Solution 2: Use loop local variable
		i := i // will shadow "i" from the "for" loop
		go func() {
			fmt.Println(i)
		}()
		/* Solution 1: Pass a parameter
		go func(n int) {
			fmt.Println(n)
		}(i)
		*/
		/* BUG: All will print 5
		go func() {
			fmt.Println(i)
		}()
		*/
	}

	time.Sleep(time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	fmt.Println(sleepSort([]int{20, 30, 10})) // [10 20 30]

	go func() { // producer
		for i := 0; i < 3; i++ {
			ch <- i
		}
		close(ch)
	}()

	// consumer
	for i := range ch {
		fmt.Println("i:", i)
	}

	/* What the above "for range" does
	for {
		i, ok := <-ch
		if !ok {
			break
		}
		fmt.Println("i:", i)
	}
	*/

	n := <-ch // ch is closed
	fmt.Println("n:", n)
	n, ok := <-ch // ch is closed
	fmt.Println("n:", n, "ok:", ok)

	// ch <- 9 // panic (ch is closed)
}

/* Channel Semantics
- send/receive will block until opposite operation(*)
	- buffered channel has N non-blocking sends
- receive from a closed channel will get zero value without blocking
	- Use ", ok" to find out if the channel is closed
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/

/*
for every number "n" in values, spin a goroutine that will
  - sleep n milliseconds
  - send n over a channel

collect all values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	ch := make(chan int)

	for _, n := range values {
		// n := n
		go func(val int) {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- val
		}(n)

		/*
			fn := func() {
				time.Sleep(time.Duration(n) * time.Millisecond)
				ch <- n
			}
			go fn()
		*/
	}

	var out []int
	for range values {
		val := <-ch
		out = append(out, val)
	}
	return out
}
