package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
)

func main() {
	// deferDemo()

	if err := killServer("app.pid"); err != nil {
		for e := err; e != nil; e = errors.Unwrap(e) {
			fmt.Println(">", e)
		}
		if errors.Is(err, fs.ErrNotExist) {
			fmt.Println("+ missing file")
		}

		log.Fatalf("error: %s", err)
	}
	// See runtime.Caller for file/line information
}

func killServer(pidFile string) error {
	file, err := os.Open(pidFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("warning: can't close %q - %s", pidFile, err)
		}
	}()

	var pid int
	if _, err := fmt.Fscanf(file, "%d", &pid); err != nil {
		return fmt.Errorf("can't get pid from %q - %w", pidFile, err)
	}

	fmt.Printf("killing %d\n", pid) // simulate kill
	if err := os.Remove(pidFile); err != nil {
		log.Printf("warning: can't delete %q - %s", pidFile, err)
	}

	return nil
}

func deferDemo() {
	defer fmt.Println("defer 1")
	defer fmt.Println("defer 2")
	fmt.Println("func body")
	return
	fmt.Println("func body 2")
}
