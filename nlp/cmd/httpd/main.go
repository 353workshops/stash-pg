package main

import (
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"github.com/ardanlabs/nlp"
	"github.com/ardanlabs/nlp/stemmer"
)

var (
	tokCalls = expvar.NewInt("tokenize.calls")
)

/*
Configuration:
- Environment: os.Getenv, ardanlabs/conf
- Command line: flag, cobra
- Configuration files: YAML, TOML, viper
*/

func main() {
	// routing
	r := chi.NewRouter()
	r.Get("/health", healthHandler)
	r.Post("/tokenize", tokenizeHandler)
	r.Get("/stem/{word}", stemHandler)
	http.Handle("/", r)
	/*
		http.handlefunc("/health", healthhandler)
		http.handlefunc("/tokenize", tokenizehandler)
	*/

	addr := ":8080"
	log.Printf("INFO: server starting on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatalf("error: can't run on %s - %s", addr, err)
	}
}

func stemHandler(w http.ResponseWriter, r *http.Request) {
	word := chi.URLParam(r, "word")
	if word == "" {
		http.Error(w, "missing word", http.StatusBadRequest)
		return
	}

	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)
}

/* Exercise:
Write a tokenizeHandler that will get the text to tokenize in the
request body.
It should return a JSON in the format:
{"tokens": ["who", "on", "first"]}

curl -d"Who's on first?" http://localhost:8080/tokenize
See also:
	- postman
	- insomnia
	- VSCode REST client extension
*/

func tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	tokCalls.Add(1)

	// Step 1: Get + Parse + Validate
	data, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "can't read", http.StatusBadRequest)
		return
	}
	text := string(data)

	// Step 2: Work
	tokens := nlp.Tokenize(text)

	// Step 3: Send Output
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	reply := map[string]any{
		"tokens": tokens,
	}
	enc.Encode(reply)
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	fmt.Fprintln(w, "OK")
}
