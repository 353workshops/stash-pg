package nlp

import (
	"os"
	"testing"
	"unicode"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

/*
var tokenizeCases = []struct {
	text   string
	tokens []string
}{
	{"", nil},
	{"Who's on first?", []string{"who", "s", "on", "first"}},
}
*/

type testCase struct {
	Text   string
	Tokens []string
	Name   string
}

func (t testCase) TestName() string {
	if t.Name != "" {
		return t.Name
	}
	return t.Text
}

func loadTokenizeCases(t *testing.T) []testCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err)
	defer file.Close()

	var cases []testCase
	dec := yaml.NewDecoder(file)
	err = dec.Decode(&cases)
	require.NoError(t, err, "parse YAML")
	return cases
}

// Exercise: Read test cases from tokenize_cases.yml
// Instead of in-memory slice of tokenizeCases
// YAML: gopkg.in/yaml.v3
func TestTokenizeTable(t *testing.T) {
	for _, tc := range loadTokenizeCases(t) {
		t.Run(tc.TestName(), func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Tokens, tokens)
		})
	}
}

func TestTokenize(t *testing.T) {
	text := "What's on second?"
	expected := []string{"what", "on", "second"}
	tokens := Tokenize(text)
	require.Equal(t, expected, tokens)
	/* before testify
	if !reflect.DeepEqual(expected, tokens) {
		t.Fatalf("expected: %#v, got: %#v", expected, tokens)
	}
	*/
}

func isCI() bool {
	// Jenkins: BUILD_NUMBER
	return os.Getenv("CI") != ""
}

func TestInCI(t *testing.T) {
	if !isCI() {
		t.Skip("not in CI")
	}
	t.Log("CI test runnig")
}

func FuzzTokenize(f *testing.F) {
	f.Add("0")
	fn := func(t *testing.T, text string) {
		tokens := Tokenize(text)
		for _, tok := range tokens {
			if !isWord(tok) {
				t.Fatalf("bad token: %q", tok)
			}
		}
	}
	f.Fuzz(fn)
}

func isWord(tok string) bool {
	for _, r := range tok {
		if !unicode.IsLower(r) {
			return false
		}
	}
	return true
}
