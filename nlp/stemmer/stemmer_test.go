package stemmer_test

import (
	"fmt"

	"github.com/ardanlabs/nlp/stemmer"
)

func ExampleStem() {
	words := []string{
		"works",
		"worked",
		"working",
	}

	for _, w := range words {
		fmt.Printf("%s -> %s\n", w, stemmer.Stem(w))
	}

	// Output:
	// works -> work
	// worked -> work
	// working -> work
}
