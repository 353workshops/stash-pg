package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ch1, ch2 := make(chan string), make(chan int)

	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- "one"
	}()

	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- 2
	}()

	/*
		done := make(chan byte)
		cancel := func() { close(done) }
		go func() {
			time.Sleep(5 * time.Millisecond)
			cancel()
		}()
	*/

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Printf("got %v from ch1\n", val)
	case val := <-ch2:
		fmt.Printf("got %v from ch2\n", val)
	case <-ctx.Done():
		fmt.Println("done")

		/*
			case <-time.After(50 * time.Millisecond):
				fmt.Println("timeout")
			case <-done:
				fmt.Println("cancel")
		*/
	}
}
