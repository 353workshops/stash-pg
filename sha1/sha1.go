package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
)

func main() {
	fmt.Println(fileSHA1("http.log.gz"))
}

// Exercise: Only decompress the file if it ends with .gz
// Otherwise, calculate the sha1 of the original content
// cat http.log.gz| gunzip | sha1sum
// cat sha1.go | sha1sum
func fileSHA1(fileName string) (string, error) {
	// cat http.log.gz
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// | gunzip
	r, err := gzip.NewReader(file)
	if err != nil {
		return "", err
	}

	// | sha1sum
	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", err
	}

	sig := w.Sum(nil)
	return fmt.Sprintf("%x", sig), nil
}
