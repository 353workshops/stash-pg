package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

func main() {
	urls := []string{
		"https://google.com",
		"https://ibm.com",
		"https://intel.com",
	}

	start := time.Now()
	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, url := range urls {
		// wg.Add(1)
		url := url
		go func() {
			defer wg.Done()
			checkURL(url)
		}()
	}
	wg.Wait()
	fmt.Println(time.Since(start))
}

func checkURL(url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("error: can't get %q - %s", url, err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		log.Printf("error: bad status for %q - %s", url, resp.Status)
		return
	}

	log.Printf("info: %q OK", url)
}
