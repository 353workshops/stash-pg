package main

import "fmt"

func main() {
	var cart []string // cart is a slice of strings
	fmt.Printf("cart = %#v of type %T\n", cart, cart)

	fmt.Println("len(cart) = ", len(cart))

	cart = []string{"lemons", "apples", "mangos", "bread"}
	fmt.Println("len(cart) = ", len(cart))
	fmt.Println(cart[1])

	fruit := cart[:3] // slicing (half-open interval)
	fmt.Println(fruit)

	cart[1] = "pears"
	fmt.Println(fruit)

	cart = append(cart, "butter")
	fmt.Println(cart)
	cart[1] = "apples"
	fmt.Println("cart:", cart)
	fmt.Println("fruit:", fruit)

	var s []int
	for i := 0; i < 10_000; i++ {
		s = appendInt(s, i)
	}
	fmt.Println(s[:10])

	/*
		values := []int{1, 2, 3}
		values[3] = 4 // panic
	*/

	arr := [3]int{10, 20, 30} // arr is an array
	fmt.Printf("arr = %#v of %T\n", arr, arr)

	s1 := []int{1, 2}
	s2 := []int{3, 4, 5}
	fmt.Println("concat:", concat(s1, s2)) // [1 2 3 4 5]

	for i := range fruit { // indices
		fmt.Println(i)
	}

	for i, f := range fruit { // index + value
		fmt.Println(i, f)
	}

	for _, f := range fruit { // values
		fmt.Println(f)
	}

	bill := []LineItem{
		{"bread", 2.3},
		{"butter", 3.4},
	}
	// discount
	for _, li := range bill { // by value
		// li is a copy of what's in bill
		li.Price *= 0.9
	}
	fmt.Println(bill)

	for i := range bill { // by reference
		bill[i].Price *= .9
	}
	fmt.Println(bill)
}

type LineItem struct {
	SKU   string
	Price float64
}

func concat(s1, s2 []int) []int {
	// FIXME: No "for" loops
	s := make([]int, len(s1)+len(s2))
	copy(s, s1)
	copy(s[len(s1):], s2)
	return s
}

func appendInt(s []int, val int) []int {
	idx := len(s)
	if len(s) < cap(s) { // enough space in underlying array
		s = s[:len(s)+1]
	} else { // need to allocate more memory
		size := 2 * (len(s) + 1)
		fmt.Println(len(s), "->", size)
		s1 := make([]int, size)
		copy(s1, s)
		s = s1[:len(s)+1]
	}
	s[idx] = val
	return s
}
