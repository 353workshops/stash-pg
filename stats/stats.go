package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(Max([]int{1, 3, 2}))
	fmt.Println(Max([]int(nil)))
	fmt.Println(Max([]float64{1, 3, 2}))
	fmt.Println(Max([]time.Month{1, 3, 2}))

	const n = 7
	var i int = n
	var f float64 = n
	fmt.Println(i, f)

	var v Value = 7
	fmt.Println(v)
	// fmt.Println(v == time.March) // won't compile
}

type Value int

func (r *Ring[T]) Insert(v T) {
	r.values[r.i] = v
	r.i = (r.i + 1) % len(r.values)
}

type Ring[T any] struct {
	i      int
	values []T
}

type Number interface {
	~int | ~float64 | ~string
}

func Max[T Number](values []T) (T, error) {
	if len(values) == 0 {
		var zero T
		return zero, fmt.Errorf("Max of empty slice")
	}

	max := values[0]
	for _, val := range values[1:] {
		if val > max {
			max = val
		}
	}

	return max, nil
}
